package pe.edu.esan.web;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pe.edu.esan.util.Util;


import javax.ws.rs.PathParam;

@Controller
@RequestMapping("/api/v1")
public class CalculatorController {

    private static final Logger logger = LogManager.getLogger(CalculatorController.class);

    @RequestMapping(value = "/execute", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> execute(@PathParam("nombre") String nombre) {
        logger.info("Enviaste: " + nombre);
        return new ResponseEntity<String>("Enviaste: " + nombre, null, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/calcular", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> execute(@PathParam("dato1") Float dato1,
                                          @PathParam("operacion") String operacion,
                                          @PathParam("dato2") Float dato2 ) {
        logger.info("Enviaste: " + dato1);
        logger.info("a operar: " + operacion);
        logger.info(" con: " + dato2);


        Float resultado = 0.0f;
        String error = null;
        if(operacion.equalsIgnoreCase("SUMA")){
            resultado = dato1 + dato2;
        }else if(operacion.equalsIgnoreCase("RESTA")){
            resultado = dato1 - dato2;
        }else if(operacion.equalsIgnoreCase("MULTIPLICACION")){
            resultado = dato1 * dato2;
        }else if(operacion.equalsIgnoreCase("DIVISION")){
            if(dato2==0){
                resultado = 0.0f;
                error = "imposible dividir entre cero";
            }else{
                resultado = dato1 / dato2;
            }
        }else{
            error = "OPERACION NO RECONOCIDA";
        }
        String response = (error == null)? Float.toString(resultado): error;
        logger.info(" resultado: " + response);
        return new ResponseEntity<String>(Util.buildString("Resultado:" , response), null, HttpStatus.CREATED);
    }

}