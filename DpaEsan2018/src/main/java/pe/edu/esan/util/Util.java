package pe.edu.esan.util;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.io.File;
import java.io.IOException;
import java.util.Date;



public class Util {

    public static String buildString(final String... values) {
        final StringBuilder sbr = new StringBuilder();
        for (String value : values) {
            sbr.append(value);
        }
        return sbr.toString();
    }
}
